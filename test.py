from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

PATH = "C:\Program Files (x86)\chromedriver.exe"
driver = webdriver.Chrome(PATH)

driver.get("https://www.youtube.com") # Opens up Youtube
searchQuery = driver.find_element_by_id('search') # Clicks the search bar
searchQuery.send_keys("spider man trailer") # Inputs the term into the search bar
searchButton = ActionChains(driver.find_element_by_id('search-icon-legacy').click()) # Clicks on search bar