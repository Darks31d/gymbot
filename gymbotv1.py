################################################################################
########
######## GymBot V0.5: Started 8/29/21, Finished 9/16/21 by Ulysses
########
################################################################################

import time
import getpass
import babel.numbers
import tkinter as tk
import datetime

from datetime import date
from typing import final
from babel.dates import TIMEDELTA_UNITS
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from tkcalendar import Calendar
from tkcalendar import DateEntry

##################
### SETUP
##################

PATH = r'C:\Program Files\\chromedriver.exe'

driver = webdriver.Chrome(PATH)

# 0 is HSC Fitness Center, 1 is Lyon Center, 3 is USC Village Fitness Center, 4 is UAC Lap Swim
gymList = ["HSC Fitness Center", "Lyon Center", "USC Village Fitness Center", "UAC Lap Swim"]

dateSelected = ""
timeSelected = ""

listOfTimes = []


##################
# START SELECTION
##################

def startUp():

    def convertDate():
        global dateSelected  ### Sets the global variable for the getGym func

        dateSelected = str(calender.get_date()) ### Gets the date
        #print("DATE: " + dateSelected) ### Prints the date



    def getGym():

        gymSelected = gymVar.get() ### Gets the selected Gym

        userName = idEnter.get() ### Gets the entered USCID
        passWord = passwordEnter.get() ### Gets the entered password

        timeSelected = timeEnter.get() ### Gets the entered time

        convertDate() ### Converts the date


        if gymSelected == "HSC Fitness Center": ### HSC
            driver.get("https://myrecsports.usc.edu/booking/da10f614-05eb-4488-a884-330ba51b4aca")
    
        elif gymSelected == "Lyon Center": ### Lyon
            driver.get("https://myrecsports.usc.edu/booking/8539d005-9387-4eb2-97a8-68bde51342cf")

        elif gymSelected == "USC Village Fitness Center": ### Village
            driver.get("https://myrecsports.usc.edu/booking/cd93ade2-af9d-4e5f-84e0-06e10711b5ce")
    
        elif gymSelected == "UAC Lap Swim": ### Swim
            driver.get("https://myrecsports.usc.edu/booking/3e44b782-a2ae-44ff-be7e-2b25230e75d1")

        elif gymSelected == "Select a Gym":
        #    print("you fucked up")
            driver.close()
    
        driver.set_window_position(0, 0) ### Puts the window on the side of the screen
        driver.set_window_size(700, 900) ### Sets the window side so it can click the dropdown menu

        WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//button[@title='Description']"))).click() ### Wait until the Login button pops up on the gym page

        userNameInput = driver.find_element_by_id('username') ### Gets the username input slot
        userNameInput.send_keys(userName) ### Passes the username into said input slot

        passWordInput = driver.find_element_by_id('password') ### Gets the password input slot
        passWordInput.send_keys(passWord) ### Passes the password into said password slot

        signInButton = driver.find_element_by_xpath('//button[@name="_eventId_proceed"]') ### Find the submit button
        signInButton.click() ### Click it

        getDate(dateSelected, timeSelected) ### Run the getDate function, with the date and time passed to it




    #### TK WINDOW

    selectionWindow = tk.Tk()
    selectionWindow.geometry("500x400")
    title = tk.Label(text= "Welcome to GymBot!")
    title.pack()
    credits = tk.Label(text = "Made by Wooden Horse Productions")
    credits.pack(side = 'bottom')

    idLabel = tk.Label(selectionWindow, text = "ID (Ex: jsmith):")
    idLabel.pack()
    idEnter = tk.Entry(selectionWindow)
    idEnter.pack()

    passwordLabel = tk.Label(selectionWindow, text = "Password:")
    passwordLabel.pack()
    passwordEnter = tk.Entry(selectionWindow)
    passwordEnter.config(show="*");
    passwordEnter.pack()

    calLabel = tk.Label(selectionWindow, text = "Select a Date:")
    calLabel.pack()
    calender = DateEntry(selectionWindow, locale='en_US', date_pattern='mm/dd/y')  # custom formatting
    calender.pack()
    calender.bind("<<DateEntrySelected>>", convertDate)

    timeLabel = tk.Label(selectionWindow, text = "The time you want to work out. Format as XX:XX AM/PM for times like 10:00 AM, and X:XX AM/PM for times like 1:00 PM:", wraplength = 360)
    timeLabel.pack()
    timeEnter = tk.Entry(selectionWindow)
    timeEnter.pack()

    gymVar = tk.StringVar(selectionWindow)
    gymVar.set("Select a Gym")
    gymListMenu = tk.OptionMenu(selectionWindow, gymVar, *gymList)
    gymListMenu.pack()

    goBtn = tk.Button(selectionWindow, text = 'Run Bot', bd = '5', command = getGym)
 
    # Set the position of button on the top of window.  
    goBtn.pack(side = 'bottom')  

    selectionWindow.mainloop()

def getDate(dateSelected, timeSelected): 

    #print("Date 2: " + dateSelected) ### Print the date to ensure it's right

    year = "" + dateSelected[0] +  dateSelected[1] +  dateSelected[2] +  dateSelected[3] ### Gets the year from the first 4 characters
    #print("Year: " + year) ### Print



    if dateSelected[8] == "0": ### If the month is before October, the format will be 0X. This function therefore only gets X and doesn't take in the 0
        day = "" + dateSelected[9]

    elif dateSelected[8] != "0": ### If the function is october or after, the format will be XX. This function therefore gets both values
        day = "" + dateSelected[8] + dateSelected[9]

    #print("Day: " + day)


    
    if dateSelected[5] == "0": ### Does the same thing as the above for the day
        month = "" + dateSelected[6]
    
    elif dateSelected[5] != "0":
        month = "" + dateSelected[5] + dateSelected[6]

    datetime_object = datetime.datetime.strptime(month, "%m") ### Gets the month number string, Ex, 9 (for September)
    month_name = datetime_object.strftime("%b") ### Converts it into the three letter abbreviation. Ex, if i = 9, then i = Sep
    #print("Month: " + month_name)
    

    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.ID, 'spanSelectedDate'))).click() ### Opens up the drop down menu

    dateButtonPath = '//button[@data-date-text="' + month_name + " " + day + ", " + year + '"]' ### Creates an xpath out of all the date data
    #print("Button Path: " + dateButtonPath)
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, dateButtonPath))).click() ### Finds the right button based off of said date xpath

    applyButton = driver.find_element_by_xpath('//button[@onclick="ApplySelectedDate()"]') ### Finds the confirm button
    applyButton.click() ### Clicks it

    getTime(timeSelected, dateSelected) ### Runs the getTime function while passing through the selected time from above





def getTime(timeSelected, dateSelected):

    driver.implicitly_wait(3) ### Wait a few seconds


    #########################################################################
    #### CONVERTING THE SELECTED TIME
    #########################################################################

    selectedTimeConverted = 0

    t1 = timeSelected
    t2 = t1.replace(":", "")
    #print("t2: " + t2)

    isAM = False

    if "AM" in t2 and "PM" not in t2: # AM, not PM
        isAM = True
        #print("It's AM")
    elif "PM" in t2 and "AM" not in t2: # PM not AM
        isAM = False
        #print("It's PM")


    startTime = t2.partition(" ")[0] ### Finds the first time in the string by going until there's a space

    if len(startTime) == 1 or len(startTime) == 3: ### If there is 1 character or 3 charactesr (One hour digit)
        hourTime = int(startTime[0]) ### Gets the hour
    elif len(startTime) == 2 or len(startTime) == 4: ### If there's two or 4 characters (2 hour digits, so like 10 11 or 12)
        hourTwo = int(startTime[1]) ## Gets second digit
        finalHour = 10 + hourTwo ### Put em together and what do you got?
        hourTime = finalHour ### Bippoty Boppity the hour
    
    hourTime *= 60

    if len(startTime) == 3:
        minuteIndexOne = 1
        minuteIndexTwo = 2
    
    elif len(startTime) == 4:
        minuteIndexOne = 2
        minuteIndexTwo = 3
    
    if len(startTime) > 1:

        minuteOne = int(startTime[minuteIndexOne]) ### Gets first digit of minute
        minuteOne *= 10
        #print("Minute 1 multiplied is: " + str(minuteOne))
        minuteTwo = int(startTime[minuteIndexTwo]) ### Gets second digit of minute
        finalMinute = minuteOne + minuteTwo ### Put em together and what do you got?
        #print("Final minute: " + str(finalMinute))
        hourTime += finalMinute ### Bippoty Boppity the final minute

    if isAM == False:
        hourTime += 720 ### Adds in 720 (60*12) since this is for the afternoon so 720 minutes have already gone by.
        #print("Hour time in minutes after adding for 12 hours: " + str(hourTime))
    elif isAM == True:
        pass
        #print("Not PM")
              
    selectedTimeConverted = hourTime ### Sets the final time to the hour time for print purposes rn

    print("Selected time in minutes: " + str(selectedTimeConverted))



    #########################################################################
    #### SETS UP RANDOM STUFF
    #########################################################################
    
    hasReservation = False

    count = 0

    #########################################################################
    #### GETS LIST OF BUTTONS
    #########################################################################

    listOfTimeButtons = driver.find_elements_by_xpath('//button[@onclick="Reserve(this)"]') ### Finds all the available time slots that aren't fully booked

    for i in listOfTimeButtons:
        listOfTimes.append(i.get_attribute('data-slot-text')) ### Gets the times from said slots
    
    #########################################################################
    #### GETS RESERVATION
    #########################################################################
    
    while hasReservation == False: ### If reservation is false, keep running this thing
        try:
            
            newButtons = driver.find_elements_by_xpath('//button[@onclick="Reserve(this)"]') ### Finds all the available time slots that aren't fully booked
            listOfFinalTimes = []

            for n in newButtons:
                if n not in listOfTimeButtons:
                    listOfTimes.append(i.get_attribute('data-slot-text')) ### Gets the times from said slots
            

            print(listOfTimes)

            for a in listOfTimes:
                n1 = a;
                n2 = n1.replace(":", "")
                n3 = n2.replace("-", "")

                print("Currently on time: " + n3)

                isPM = False

                if "AM" in n3 and "PM" not in n3: # AM, not PM
                    isPM = False
                    print("It's AM")
                elif "PM" in n3 and "AM" not in n3: # PM not AM
                    isPM = True
                    print("It's PM")
                elif "PM" in n3 and "AM" in n3: # Both PM and AM in it. Defaults to AM since that's where the first starting time will always be.
                    isPM = False
                    print("It's got both in it, but it's really AM")


                startTime = n3.partition(" ")[0] ### Finds the first time in the string by going until there's a space

                if len(startTime) == 1 or len(startTime) == 3: ### If there is 1 character or 3 charactesr (One hour digit)
                    hourTime = int(startTime[0]) ### Gets the hour
                elif len(startTime) == 2 or len(startTime) == 4: ### If there's two or 4 characters (2 hour digits, so like 10 11 or 12)
                    hourTwo = int(startTime[1]) ## Gets second digit
                    finalHour = 10 + hourTwo ### Put em together and what do you got?
                    hourTime = finalHour ### Bippoty Boppity the hour
    
                hourTime *= 60

                if len(startTime) == 3:
                    minuteIndexOne = 1
                    minuteIndexTwo = 2
    
                elif len(startTime) == 4:
                    minuteIndexOne = 2
                    minuteIndexTwo = 3

                if len(startTime) > 1:
                    minuteOne = int(startTime[minuteIndexOne]) ### Gets first digit of minute
                    minuteOne *= 10
                    print("Minute 1 multiplied is: " + str(minuteOne))
                    minuteTwo = int(startTime[minuteIndexTwo]) ### Gets second digit of minute
                    finalMinute = minuteOne + minuteTwo ### Put em together and what do you got?
                    print("Final minute: " + str(finalMinute))
                    hourTime += finalMinute ### Bippoty Boppity the final minute

                if isPM == True:
                    hourTime += 720 ### Adds in 720 (60*12) since this is for the afternoon so 720 minutes have already gone by.
                    print("Hour time in minutes after adding for 12 hours: " + str(hourTime))
                elif isPM == False:
                    print("Not PM")
              
                finalTime = hourTime ### Sets the final time to the hour time for print purposes rn

                print("The final time is: " + str(finalTime))

                listOfFinalTimes.append(finalTime)

            

            ### Basically, just need to 1. Add times to a list. 2. In a while loop, find the closest one. Then try to reserve that time. If you make it, cool, exits the program. If you don't, then delete that time from the array and try the next closest.
            ### 3. If the closest time is more than an hour away from the selected, then just clears the array, and rechecks the times. Does this until you make it.

            print("Time I want to be closest to: " + str(selectedTimeConverted))

            for x in listOfFinalTimes:
                difInSize = abs(selectedTimeConverted - x)

                print(str(difInSize))

                if difInSize < 60 or difInSize == 60: # RESERVE IT!

                    timeIndex = listOfFinalTimes.index(x)

                    timeToReserve = listOfTimeButtons[timeIndex]

                    testButton = timeToReserve

                    testButton.click()

                    time.sleep(3)

                    driver.quit()

                elif difInSize > 60:
                    print("Run it again!")
            
            testButton = driver.find_element_by_class_name("doesn't exist") ### This always returns false, so it causes the loop to be reset
        except:
            time.sleep(6) ### Waits so you're not just trying like every millisecond. Divide 60 by this and that's how many times it runs per minute
            listOfFinalTimes.clear()
            count += 1
            print("Number of times attempted: " + str(count))
            

startUp()
